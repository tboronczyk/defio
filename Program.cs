﻿using System;

namespace gameproject
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            Console.WriteLine("Score: {0}\tLevel: {1}", game.Score, game.Level);
        }
    }
}
