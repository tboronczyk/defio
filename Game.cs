using System;

namespace gameproject
{
    class Game
    {
        public int Score
        { get; set; }
        public int Level
        { get; set; }

        public Game()
        {
            Score = 0;
            Level = 0;
        }
    }
}
